package net.downwithdestruction.dwdgmlogger.listeners;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;

import net.downwithdestruction.dwdgmlogger.DwDGMLogger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerGameModeChangeEvent;

public class GameModeListener implements Listener {
	@EventHandler()
	public void onGameModeChange(PlayerGameModeChangeEvent e) {
		Player p = e.getPlayer();
		if (p == null) return;
		
		// Compile MESSAGE
		String gamemode = (p.getGameMode() == GameMode.SURVIVAL) ? "CREATIVE" : "SURVIVAL"; // This fixes the reversed output 
		int x = (int) p.getLocation().getX();
		int y = (int) p.getLocation().getY();
		int z = (int) p.getLocation().getZ();
		String message = p.getName() + "'s gamemode has changed to " + gamemode + " ([" + p.getWorld().getName() + "]: " + x + ", " + y + ", " + z + ").";
		String alert = ChatColor.BOLD + " " +
				ChatColor.GREEN +      "*" +
				ChatColor.GOLD +       "*" +
				ChatColor.YELLOW +     "*" +
				ChatColor.RED +        "A" +
				ChatColor.DARK_RED +   "L" +
				ChatColor.RED +        "E" +
				ChatColor.DARK_RED +   "R" +
				ChatColor.RED +        "T" +
				ChatColor.DARK_RED +   "!" +
				ChatColor.YELLOW +     "*" +
				ChatColor.GOLD +       "*" +
				ChatColor.GREEN +       "*" +
				ChatColor.RESET +      " " +
				ChatColor.GREEN;
		
		// Output to CONSOLE
		DwDGMLogger.getInstance().getServer().getConsoleSender().sendMessage("[DwDGMLogger] " + alert + message);
		
		// Output to ALL online OPS
		for (Player t : DwDGMLogger.getInstance().getServer().getOnlinePlayers()) {
			if (t.isOp()) {
				t.sendMessage(alert + message);
			}
		}
		
		// Output to LOG FILE
		File log = new File(DwDGMLogger.getInstance().getDataFolder() + File.separator + "gamemodechange.log");
		try {
			if (!log.exists()) {
				log.getParentFile().mkdirs();
				log.createNewFile();
			}
			BufferedWriter out = new BufferedWriter(new FileWriter(log, true));
			SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
			out.write(dateFormatGmt.format(new Date()) + " " +message);
			out.newLine();
			out.close();
		} catch (IOException ioe) {
			Bukkit.getLogger().log(Level.INFO, "[DwDGMLogger] " + "Failed to save to chat log file " + log.getAbsolutePath() + " : " + ioe.getMessage());
		}
	}
}
