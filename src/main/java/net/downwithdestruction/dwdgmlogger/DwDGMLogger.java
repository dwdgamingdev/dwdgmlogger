package net.downwithdestruction.dwdgmlogger;

import java.util.logging.Level;

import net.downwithdestruction.dwdgmlogger.listeners.GameModeListener;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class DwDGMLogger extends JavaPlugin {
	private final PluginManager pluginManager = Bukkit.getPluginManager();
	private static DwDGMLogger instance;
	
	public DwDGMLogger() {
		instance = this;
	}
	
	public static DwDGMLogger getInstance() {
		return instance;
	}
	
	public void onEnable() {
		pluginManager.registerEvents(new GameModeListener(), this);
		Bukkit.getLogger().log(Level.INFO, "[DwDGMLogger] v" + this.getDescription().getVersion() + " by BillyGalbreath enabled!");
	}
	
	public void onDisable() {
		Bukkit.getLogger().log(Level.INFO, "[DwDGMLogger] Plugin Disabled.");
	}
}
